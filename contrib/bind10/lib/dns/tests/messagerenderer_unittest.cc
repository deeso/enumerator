// Copyright (C) 2009  Internet Systems Consortium, Inc. ("ISC")
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS.  IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
// LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
// OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
// PERFORMANCE OF THIS SOFTWARE.

// $Id: messagerenderer_unittest.cc 1566 2010-03-19 02:13:32Z jreed $

#include <vector>

#include <dns/buffer.h>
#include <dns/name.h>
#include <dns/messagerenderer.h>

#include "unittest_util.h"

#include <gtest/gtest.h>

using isc::UnitTestUtil;
using isc::dns::OutputBuffer;
using isc::dns::Name;
using isc::dns::MessageRenderer;

namespace {
class MessageRendererTest : public ::testing::Test {
protected:
    MessageRendererTest() : expected_size(0), buffer(0), renderer(buffer)
    {
        data16 = (2 << 8) | 3;
        data32 = (4 << 24) | (5 << 16) | (6 << 8) | 7;
    }
    size_t expected_size;
    uint16_t data16;
    uint32_t data32;
    OutputBuffer buffer;
    MessageRenderer renderer;
    std::vector<unsigned char> data;
    static const uint8_t testdata[5];
};

const uint8_t MessageRendererTest::testdata[5] = {1, 2, 3, 4, 5};

// The test cases are borrowed from those for the OutputBuffer class.
TEST_F(MessageRendererTest, writeIntger)
{
    renderer.writeUint16(data16);
    expected_size += sizeof(data16);

    EXPECT_PRED_FORMAT4(UnitTestUtil::matchWireData, renderer.getData(),
                        renderer.getLength(), &testdata[1], sizeof(data16));
}

TEST_F(MessageRendererTest, writeName)
{
    UnitTestUtil::readWireData("testdata/name_toWire1", data);
    renderer.writeName(Name("a.example.com."));
    renderer.writeName(Name("b.example.com."));
    renderer.writeName(Name("a.example.org."));
    EXPECT_PRED_FORMAT4(UnitTestUtil::matchWireData, buffer.getData(),
                        buffer.getLength(), &data[0], data.size());
}

TEST_F(MessageRendererTest, writeNameInLargeBuffer)
{
    size_t offset = 0x3fff;
    buffer.skip(offset);

    UnitTestUtil::readWireData("testdata/name_toWire2", data);
    renderer.writeName(Name("a.example.com."));
    renderer.writeName(Name("a.example.com."));
    renderer.writeName(Name("b.example.com."));
    EXPECT_PRED_FORMAT4(UnitTestUtil::matchWireData,
                        static_cast<const uint8_t*>(buffer.getData()) + offset,
                        buffer.getLength() - offset,
                        &data[0], data.size());
}

TEST_F(MessageRendererTest, writeNameWithUncompressed)
{
    UnitTestUtil::readWireData("testdata/name_toWire3", data);
    renderer.writeName(Name("a.example.com."));
    renderer.writeName(Name("b.example.com."), false);
    renderer.writeName(Name("b.example.com."));
    EXPECT_PRED_FORMAT4(UnitTestUtil::matchWireData, buffer.getData(),
                        buffer.getLength(), &data[0], data.size());
}

TEST_F(MessageRendererTest, writeNamePointerChain)
{
    UnitTestUtil::readWireData("testdata/name_toWire4", data);
    renderer.writeName(Name("a.example.com."));
    renderer.writeName(Name("b.example.com."));
    renderer.writeName(Name("b.example.com."));
    EXPECT_PRED_FORMAT4(UnitTestUtil::matchWireData, buffer.getData(),
                        buffer.getLength(), &data[0], data.size());
}

TEST_F(MessageRendererTest, writeNameCaseCompress)
{
    UnitTestUtil::readWireData("testdata/name_toWire1", data);
    renderer.writeName(Name("a.example.com."));
    // this should match the first name in terms of compression:
    renderer.writeName(Name("b.exAmple.CoM."));
    renderer.writeName(Name("a.example.org."));
    EXPECT_PRED_FORMAT4(UnitTestUtil::matchWireData, buffer.getData(),
                        buffer.getLength(), &data[0], data.size());
}

TEST_F(MessageRendererTest, writeRootName)
{
    // root name is special: it never causes compression or can (reasonably)
    // be a compression pointer.  So it makes sense to check this case
    // explicitly.
    Name example_name = Name("www.example.com");

    OutputBuffer expected(0);
    expected.writeUint8(0);     // root name
    example_name.toWire(expected);

    renderer.writeName(Name("."));
    renderer.writeName(example_name);
    EXPECT_PRED_FORMAT4(UnitTestUtil::matchWireData,
                        static_cast<const uint8_t*>(buffer.getData()),
                        buffer.getLength(),
                        static_cast<const uint8_t*>(expected.getData()),
                        expected.getLength());
}
}
