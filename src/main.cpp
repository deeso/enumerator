/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include <dns/buffer.h>
#include <dns/question.h>
#include <dns/message.h>
#include <dns/messagerenderer.h>
#include <dns/rrset.h>

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

#include "QuerySender.h"
#include "tspriorityqueue.h"
#include "ParseIPAddress.h"


char *data = "\x03www\x07sockets\x03com\x00\x00\x01\x00\x01";
std::string sdata(data, 21);

void testIPAddressParsing(){
	std::string x("192.168.1.0/25");
	UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4Addresses> ips_queue;
	if(ParseIPAddress::Inst()->expandCidrAddress(&ips_queue, x)){
		while (!ips_queue.isEmpty()){
			std::cout << ips_queue.popTop();
		}
	}
	std::string y("128.168.1.0/16");
	boost::thread * async =  ParseIPAddress::Inst()->expandCidrAddressAsync(&ips_queue, y);
	if(async == nullptr){
		std::cout << "Fail!" << std::endl;
	}
	std::string z("172.168.1.0/8");
	if(ParseIPAddress::Inst()->expandCidrAddress(&ips_queue, z)){
		while (!ips_queue.isEmpty()){
			ips_queue.clear();
		}
	}
	std::string a("119.168.1.08");
	if(ParseIPAddress::Inst()->expandCidrAddress(&ips_queue, a)){
		while (!ips_queue.isEmpty()){
			ips_queue.clear();
		}
	}
	std::string b("129.168.1.0//8");
	if(ParseIPAddress::Inst()->expandCidrAddress(&ips_queue, b)){
		while (!ips_queue.isEmpty()){
			ips_queue.clear();
		}
	}

}

void query_nameserver(std::string &nameserver, std::string &basedomain,std::vector<std::string> hosts, std::vector<std::string> addresses, std::string outfile ){

	UniqueTSPQueue<std::string> unverified_names_queue;
	UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4_NamesAddresses> verified_hosts_queue;
	UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4Addresses> unverified_ips_queue;
	std::ofstream out;
	out.open(outfile.c_str(), std::ios::app);

	std::vector<std::string>::iterator iter = hosts.begin();
	for (;iter != hosts.end(); iter++){
		unverified_names_queue >> *iter;
	}

	std::vector<boost::thread *> parse_threads;
	iter = addresses.begin();
	for(;iter != addresses.end(); iter++){
		//boost::thread *t = ParseIPAddress::Inst()->expandCidrAddressAsync(&unverified_ips_queue, *iter);
		//parse_threads.push_back(t);
		ParseIPAddress::Inst()->expandCidrAddress(&unverified_ips_queue, *iter);
	}
	QuerySender qsender(nameserver,
						basedomain,
						verified_hosts_queue,
						unverified_names_queue, 
						unverified_ips_queue,
						53, // port
						10, // ip boundary
						100, // # of pending queries
						101, // staring query id
						3, // max # of retrys
						1000); // number of milliseconds to wait for before sending
	boost::thread * t = qsender.start();
	std::string host;
	while (1){
		host = "";
		while(!verified_hosts_queue.isEmpty()){
			verified_hosts_queue << host;
			//std::cout << "Found: " << host << std::endl;
			out << host << std::endl;
		}
		if (!qsender.isQueriesPending() && 
			unverified_ips_queue.isEmpty() && 
			unverified_names_queue.isEmpty())
			break;
	}
	qsender.stop();
	t->join();
	delete t;
	host = "";
	while(!verified_hosts_queue.isEmpty()){
		verified_hosts_queue << host;
		//std::cout << "Found: " << host << std::endl;
		out << host << std::endl;
	}
}


int main(){
	//testIPAddressParsing();
	std::vector<std::string> nameservers;
	nameservers.push_back("ns1.somenameserver.com");
	std::string x("192.168.0.0/16");
	UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4Addresses> ips_queue;
	//if(!ParseIPAddress::Inst()->expandCidrAddress(&ips_queue, x)){
	//	std::cout << "Had problems expanding 192.168.0.0/16" << std::endl;
	//	return -1;
	//}
	std::vector<std::string> hosts;
	std::string data;
	//std::fstream input("C:\\enumerator\\hosts.txt", std::ios::in);
	//while (input >> data){
	//	hosts.push_back(data);
	//}
	
	std::vector<std::string> addresses;
	addresses.push_back("192.168.0.0/16");
	std::string basedomain = "someplace.com";
	std::vector<std::string>::iterator iNameserver = nameservers.begin();
	for(;iNameserver != nameservers.end(); iNameserver++){
		query_nameserver(*iNameserver, basedomain, hosts, addresses, "S:\\qsender_results\\"+*iNameserver+"_internal_ips.txt");
	}
	
	return 0;
}



