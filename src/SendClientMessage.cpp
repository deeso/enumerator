/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
 
#include <string>
#include <iostream>

#include <dns/buffer.h>
#include <dns/question.h>
#include <dns/message.h>
#include <dns/messagerenderer.h>
#include <dns/rrset.h>

#include <boost\shared_ptr.hpp>
#include <boost\asio.hpp>
#include <boost\bind.hpp>
#include <boost\regex.hpp>
#include <boost\thread\condition_variable.hpp>
#include <boost\thread\mutex.hpp>
#include <boost\date_time\posix_time\posix_time.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <boost/interprocess/sync/interprocess_upgradable_mutex.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>


#include "tspriorityqueue.h"
#include "QuerySender.h"
#include "SendClientMessage.h"

void SendClientMessage::cancel_timer(){
	boost::mutex::scoped_lock sl(timer_mutex_);
	timer_.cancel();
	//if(timer_.cancel() > 0)
	//	timer_cnd_.wait(sl);
}
void SendClientMessage::cancel_complete(){
	boost::mutex::scoped_lock sl(timer_mutex_);
	timer_cnd_.notify_all();
}
SendClientMessage::SendClientMessage(boost::asio::io_service& io, 
				  std::string nameserver, 
				  unsigned port,
				  QuerySender &s,
				  unsigned int time_delay,
				  unsigned int max_retrys):io_(io), 
				  				  nameserver_(nameserver),
								  socket_(io_),
								  timer_mutex_(),
								  timer_cnd_(),
								  qsender_(s),
								  ep_(boost::asio::ip::address_v4::from_string(nameserver_), 53),
								  time_delay_(time_delay),
								  max_retrys_(max_retrys),
								  timer_(io_)
{
	socket_.open(boost::asio::ip::udp::v4());
	recv_buf = new boost::array<char, isc::dns::Message::DEFAULT_MAX_UDPSIZE>();
	retrys_ = 0;
	query_pending_ = false;
	data_processed_ = true;
}
bool SendClientMessage::sendQuery(std::string data, unsigned short qid){
	boost::mutex::scoped_lock sl(mutex_);
	if (!data_processed_){
		return false;
	}else if(query_pending_){
		return false;
	}
	data_ = data;
	query_pending_ = true;
	pending_qid_ = qid;
	retrys_ = 0;
	buf_ = new boost::asio::mutable_buffer((void *)data_.c_str(), data_.size());	
	try{
		socket_.async_send_to( boost::asio::buffer(*buf_), ep_,
			boost::bind(&SendClientMessage::handleSendQuery, this,boost::asio::placeholders::error));
		socket_.async_receive_from(boost::asio::buffer(*recv_buf), ep_,
        	boost::bind(&SendClientMessage::receiveData, this));
//			timer_.expires_from_now(boost::posix_time::millisec(time_delay_));
//			timer_.async_wait(boost::bind(&SendClientMessage::handleResendTimer,this, boost::asio::placeholders::error));
		return true;
	}catch( boost::system::system_error & e) {
			std::cout << "Uh-Oh exception in receiveAsyncData: "  << e.what() << std::endl;
	} catch(const isc::dns::MessageTooShort e){
		/* need to implement read_some here */
		std::cout << e.what();
	}catch(const isc::dns::InvalidMessageSection e){
			/* need to implement read_some here */
			std::cout << e.what();
	}
	query_pending_ = false;
	return false;
}
void SendClientMessage::resendQuery(){
	boost::mutex::scoped_lock sl(mutex_);
	socket_.async_send_to( boost::asio::buffer(*buf_), ep_,
			boost::bind(&SendClientMessage::handleSendQuery, this,boost::asio::placeholders::error));
	socket_.async_receive_from(boost::asio::buffer(*recv_buf), ep_,
        	boost::bind(&SendClientMessage::receiveData, this));
//	timer_.expires_from_now(boost::posix_time::millisec(3*time_delay_));
//	timer_.async_wait(boost::bind(&SendClientMessage::handleResendTimer,this, boost::asio::placeholders::error));
}
void SendClientMessage::handleSendQuery(const boost::system::error_code& e){
	boost::mutex::scoped_lock sl(mutex_);
	try{
			timer_.expires_from_now(boost::posix_time::millisec(time_delay_));
			timer_.async_wait(boost::bind(&SendClientMessage::handleResendTimer,this, boost::asio::placeholders::error));
	}catch( boost::system::system_error & e) {
			std::cout << "Uh-Oh exception in receiveAsyncData: "  << e.what() << std::endl;
	} catch(const isc::dns::MessageTooShort e){
		/* need to implement read_some here */
		std::cout << e.what();
	}catch(const isc::dns::InvalidMessageSection e){
			/* need to implement read_some here */
			std::cout << e.what();
	}
}
void SendClientMessage::handleResendTimer(const boost::system::error_code& e){
	boost::mutex::scoped_lock sl(mutex_);
	if(e == boost::asio::error::operation_aborted){
		cancel_complete();
		//qsender_.handleClientMessageTimeout(pending_qid_);
		//query_pending_ = false;
		return;
	}
	else if (e){
		std::cout << "Uh-oh Error in handleReSendTimer: " << e.message() << std::endl;
		return;
	}
	if (!query_pending_)
		return;

	retrys_++;
	if (retrys_ == max_retrys_){
		// notify the qsender here
		//std::cout << "Max retrys hit! "<< pending_qid_ << std::endl;
		query_pending_ = false;
		qsender_.handleClientMessageTimeout(pending_qid_);
		return;
	}
	try{
		if(query_pending_){
			sl.unlock();
			resendQuery();
			return;
		}else{
			// looks like while we were preparing to send, we recieved the message.
			// nothing to do since the operation did not reschedule the timer.
			return;
		}

	}catch(boost::system::system_error & e) {
		std::cout << "Uh-Oh exception in sendDNSQuery: "  << e.what() << std::endl;
	}
//	timer_.expires_from_now(boost::posix_time::millisec(time_delay_));
//	timer_.async_wait(boost::bind(&SendClientMessage::handleResendTimer,this, boost::asio::placeholders::error));
}
void SendClientMessage::receiveData(){
	boost::mutex::scoped_lock sl(mutex_);	
	//std::cout << "******************Query Pending: "<< query_pending_ << " " <<pending_qid_ <<std::endl;
	if(!query_pending_){
		return;
	}
	isc::dns::Message *msg = new isc::dns::Message(isc::dns::Message::Mode::PARSE);
	try{
		cancel_timer();
		msg->clear(isc::dns::Message::Mode::PARSE);
		isc::dns::InputBuffer inbuffer(recv_buf->data(), recv_buf->size());
		msg->fromWire(inbuffer);
		query_pending_ = false;
		qsender_.handleClientMessageResponse(msg);
	}catch(const isc::dns::MessageTooShort e){
		/* need to implement read_some here */
		std::cout << e.what();
	}catch(const isc::dns::InvalidMessageSection e){
		/* need to implement read_some here */
		std::cout << e.what();
	}
}
void SendClientMessage::cancel(){
	boost::mutex::scoped_lock sl(timer_mutex_);
	if(timer_.cancel() > 0)
		timer_cnd_.wait(sl);
}

SendClientMessage::~SendClientMessage(){
	cancel();
}