/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#include <string>
#include <iostream>

#include <dns/buffer.h>
#include <dns/question.h>
#include <dns/message.h>
#include <dns/messagerenderer.h>
#include <dns/rrset.h>

#include <boost\shared_ptr.hpp>
#include <boost\asio.hpp>
#include <boost\bind.hpp>
#include <boost\regex.hpp>
#include <boost\thread\condition_variable.hpp>
#include <boost\thread\mutex.hpp>
#include <boost\date_time\posix_time\posix_time.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <boost/interprocess/sync/interprocess_upgradable_mutex.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>


#include "tspriorityqueue.h"
#include "QuerySender.h"

class SendClientMessage{
	boost::mutex mutex_;
	std::string data_;
	std::string nameserver_;
	boost::asio::io_service& io_;
	boost::asio::ip::udp::socket socket_;
	boost::asio::deadline_timer timer_;
	unsigned int retrys_;
	unsigned int max_retrys_;
	boost::array<char, isc::dns::Message::DEFAULT_MAX_UDPSIZE> *recv_buf;
	QuerySender &qsender_;
	boost::asio::ip::udp::endpoint ep_;
	volatile bool query_pending_;
	bool data_processed_;
	unsigned short pending_qid_;
	boost::asio::mutable_buffer *buf_;
	unsigned int time_delay_;
	unsigned short port_;
	SendClientMessage(boost::asio::io_service& io, 
		QuerySender &s):io_(io), qsender_(s),socket_(io_),timer_(io_){}
	boost::mutex timer_mutex_;
	boost::condition_variable timer_cnd_;
	bool cancel_waiting_flag;
	void cancel_timer();
	void cancel_complete();

public:
	SendClientMessage(boost::asio::io_service& io, 
					  std::string nameserver, 
					  unsigned port,
					  QuerySender &s,
					  unsigned int time_delay,
					  unsigned int max_retrys_);
	bool sendQuery(std::string data, unsigned short qid);
	void handleSendQuery(const boost::system::error_code& e);
	void handleResendTimer(const boost::system::error_code& e);
	void receiveData();
	void resendQuery();
	void dataProcessed(){ data_processed_ = true;}
	void cancel();
	bool isQueryPending(){return query_pending_;}
	~SendClientMessage();
};

