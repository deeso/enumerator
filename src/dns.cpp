/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#include <vector>
#include <string>
#include <sstream>

#include "dns.h"

void Question::parseData(char *data, unsigned int sz){
	parseData(std::string(data, sz));
}

void Question::parseData(std::string data){
	const char *pdata = data.c_str(),
				*eqdata=pdata+data.size()-4;
	unsigned int cnt = 0;
	while (pdata != '\x00' && pdata < eqdata){
		std::string s = std::string(pdata+1,*pdata);
		qname += s;
		if (*((unsigned int)*pdata + pdata) != '\x00'){
			qname += ".";
		}
		pdata += *pdata + 1;
	}
	pdata = data.c_str() + data.size()-4;
	qtype = *(unsigned short *) pdata;
	qclass = *(unsigned short *) (pdata+2);
}

Question::Question(std::string data):qname(""), qtype(0),qclass(0){
	parseData(data);
}
Question::Question(char * data, unsigned int sz):qname(""), qtype(0),qclass(0){
	parseData(data, sz);
}

std::string Question::serialize(){
	std::stringstream data;
	std::vector<std::string > *ntokens = tokenize(".");
	std::vector<std::string >::iterator be = ntokens->begin();
	for (;be != ntokens->end();be++){
		data << (unsigned char ) be->size() << be->c_str();  
	}
	
	data << '\x00' << (unsigned char) (qtype >> 8 && 0xff) << (unsigned char) (qtype && 0xff);
	data << (unsigned char) (qclass >> 8 && 0xff) << (unsigned char) (qclass && 0xff);
	
	unsigned int sz = data.tellp();
	char *cdata = new char[sz],
		 *icdata = cdata;
	data.seekp(0);
	data.get( cdata, sz);
	
	std::string result(cdata, sz);
	delete cdata;
	delete ntokens;
	return result;
}

std::vector<std::string > *Question::tokenize(std::string del){
	std::vector<std::string > *tokens = new std::vector<std::string > ;
	
	std::string::size_type lastPos = qname.find_first_not_of(del,0);
	std::string::size_type currentPos = qname.find_first_of(del,lastPos);
	while(std::string::npos != currentPos || std::string::npos != lastPos){
		std::string s = qname.substr(lastPos, currentPos - lastPos);
		tokens->push_back(s);
		lastPos = qname.find_first_not_of(del,currentPos);
		currentPos = qname.find_first_of(del,lastPos);
	}
	return tokens;
}

