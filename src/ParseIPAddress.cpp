/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/



#include <string>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

#include "ParseIPAddress.h"

#include "QuerySender.h"
#include "tspriorityqueue.h"

#ifdef UNIX
#include <arpa/inet.h> // XXX: for inet_pton/ntop(), not exist in C++ standards
#include <sys/socket.h> // for AF_INET/AF_INET6
#else
#include <Ws2tcpip.h>
#endif



ParseIPAddress::ParseIPAddress(void)
{
}


ParseIPAddress::~ParseIPAddress(void)
{
}


ParseIPAddress* ParseIPAddress::Inst(){
	static ParseIPAddress inst;
	return &inst;
}

std::string ParseIPAddress::getIPAddressString(unsigned int ip_int){
	struct sockaddr_in sa;
	char dst[1024];
	sa.sin_addr.S_un.S_addr = ntohl(ip_int);
	inet_ntop(AF_INET,&sa.sin_addr,dst,1024);
	std::string x(dst);
	return x;
}

boost::thread * ParseIPAddress::expandCidrAddressAsync(UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4Addresses>* ips_queue, std::string ip_str){
	boost::thread *async = nullptr;
	async = new boost::thread(boost::bind(&ParseIPAddress::expandCidrAddress, this, ips_queue, ip_str));
	return async;

}

bool ParseIPAddress::expandCidrAddress(UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4Addresses>* ips_queue, std::string ip_str){
	// find slash
	// if no slash return false
	// if ip and slash found attempt to parse ip address
	unsigned int slash_pos = ip_str.find_first_of("/");
	if (slash_pos == std::string::npos){
		return false;
	}
	std::string ip_addr = ip_str.substr(0,slash_pos),
		mask = ip_str.substr(slash_pos+1,ip_str.size());

	
	unsigned int addr_int = 0,
				 mask_int = 0;

	if (!getIPAddressInt(ip_addr, addr_int))
		return false;
	if (!getIPCidrMaskInt(mask, mask_int))
		return false;

	unsigned int start = addr_int & mask_int;
	unsigned int end = start &mask_int;
	do{
		++end;
		std::string x = getIPAddressString(addr_int++);
		*ips_queue >> x;
	}while ((start & (~mask_int)) != (end & (~mask_int)));
	return true;
}

bool ParseIPAddress::getIPAddressInt(std::string ip_addr, unsigned int& addr_int){
	static struct sockaddr_in sa;
	if (inet_pton(AF_INET, ip_addr.c_str(), &(sa.sin_addr)) == 1){
		addr_int = ntohl(sa.sin_addr.S_un.S_addr);
		return true;
	}
	return false;
}
bool ParseIPAddress::getIPCidrMaskInt(std::string mask, unsigned int& mask_int){
	// try X.X.X.X
	static struct sockaddr_in sa;
	unsigned int bit_cnt = 0;
	if (inet_pton(AF_INET, mask.c_str(), &(sa.sin_addr)) == 1){
		mask_int = sa.sin_addr.S_un.S_addr;
		return true;
	}
	try{
		bit_cnt  = boost::lexical_cast<unsigned int>(mask);
	}catch(boost::bad_lexical_cast &){
		mask_int = 0;
		return false;
    } 
	mask_int = 0x80000000;
	for (unsigned int i = 0; i < 32, i < bit_cnt-1; i++){
		 mask_int = mask_int >> 1;
		 mask_int+=0x80000000;
	}
	return true;
}


