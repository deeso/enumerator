/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#pragma once

#include <string>
#include <iostream>

#include <dns/buffer.h>
#include <dns/question.h>
#include <dns/message.h>
#include <dns/messagerenderer.h>
#include <dns/rrset.h>

#include <boost\shared_ptr.hpp>
#include <boost\asio.hpp>
#include <boost\bind.hpp>
#include <boost\regex.hpp>
#include <boost\thread\condition_variable.hpp>
#include <boost\thread\mutex.hpp>
#include <boost\date_time\posix_time\posix_time.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <boost/interprocess/sync/interprocess_upgradable_mutex.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>


#include "tspriorityqueue.h"


// TODO: Add an IPv4 Address Sorting structure
// TODO: Add a domain name sorting structure
std::vector<std::string > *tokenize(const std::string& str, std::string del);
void clearTokens(std::vector<std::string> *a_tokens, std::vector<std::string> *b_tokens );
bool compareByFQDN(std::string a, std::string b);
bool compareByIPv4Address(std::string a, std::string b);
bool compareByIPv4AndFQDN(std::string a, std::string b);

struct CompareIPv4Addresses{
	bool operator()(std::string a, std::string b) {
		return compareByIPv4Address(a, b);
	}
};

struct CompareNames{
	bool operator()(std::string a, std::string b) {
		return compareByFQDN(a, b);
	}
};

struct CompareIPv4_NamesAddresses{
	bool operator()(std::string a, std::string b) {
		return compareByIPv4AndFQDN(a, b);
	}
};

class SendClientMessage;

class QuerySender: public boost::enable_shared_from_this<QuerySender>,
    private boost::noncopyable
{
	std::string nameserver_;
	unsigned short port_;
	std::string base_domain_;

	bool continueSending_;

	/* 
	   reciever thread for the
	   pending queries
	*/
	boost::thread_group threads_;
	bool continue_to_recv_;
	boost::thread *rcvr_thread;
	void receiveData();
	void handleReceive(isc::dns::Message *msg);
	/* 
	   networking compenents needed by the 
	   QuerySender Class
	*/
	
	boost::mutex io_mutex_;
	boost::asio::io_service io_service_;
	boost::asio::ip::udp::socket socket_;
	boost::asio::ip::udp::endpoint remote_endpoint_;
	
	void handleSendQuery(const boost::system::error_code& e, std::size_t bytes_xfered, unsigned short qid, unsigned int bytes_snt_before);
	void cancelTimer(unsigned short qid);
	void run();
	unsigned int qsend_delay_milli_;
	//void run(const boost::asio::io_service *io_ptr);
	/* do i need a mapping of domain names to ip addresses and vice versa???  not part of the requirements.*/
	/*
	   shared queues between multiple QuerySender Objects and 
	   others 
	*/
	UniqueTSPQueue<std::string>& unverified_names_queue_;
	
	UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4_NamesAddresses>& verified_hosts_queue_;
	UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4Addresses>& unverified_ips_queue_;

	//boost::interprocess::interprocess_upgradable_mutex checked_ips_mutex_;
	//std::set<unsigned int> checked_ip_addresses_ ;
	UniqueTSPQueue<unsigned int, std::vector<unsigned int>> checked_ip_addresses_;
	void addCheckedAddrs(std::set<std::string> addrs);
	void addCheckedAddrs(std::vector<std::string> addrs);
	void addCheckedAddrs(std::string addr);
	void addCheckedAddrs(unsigned int ip_address);

	bool isAddrChecked(std::string ip_address);
	bool isAddrChecked(unsigned int ip_address);
	/*
		used internally to validate unverified ip addresses
	*/
	unsigned int ip_address_boundary_;
	
	
	/*
		Used to atomically increment the qid 
		with each query
	*/
	boost::mutex quid_mutex_;
	unsigned short current_quid_;
	unsigned short incrQuid();

	/*
		Internal objects to track pending queues and update
		everything accrodingly, when not using clients to send
		the queries
	*/
	boost::condition_variable pending_queries_cnd_;
	boost::interprocess::interprocess_upgradable_mutex pending_qmutex_;
	
	unsigned short max_pending_queries_;
	boost::mutex queries_cnt_mutex_;
	
	std::map<unsigned short, boost::asio::mutable_buffer *> pending_qdata_;
	std::map<unsigned short, boost::asio::deadline_timer *> pending_qtimers_;
	std::map<unsigned short, unsigned int> pending_qretrys_;
	
	unsigned int getPendingCount();
	void waitPendingQueries();
	void wakeupPendingQueriesThreads(const boost::system::system_error e);

	
	
	
	unsigned short max_retrys_;
	
	void receiveDataAsync();
	void handleReceiveAsync(boost::array<char, isc::dns::Message::DEFAULT_MAX_UDPSIZE> *recv_buf);
	void sendARecordQuery(std::string name);
	

	bool isValidQueryResponse(void *data, unsigned int size);
	bool isIPv4Address(std::string name);

	std::string buildNextName();
	std::string buildARecordQueryDNSMsg(isc::dns::Message &msg, std::string name, unsigned short qid=0);
	std::string buildPTRRecordQueryDNSMsg(isc::dns::Message &msg, std::string ip_address, unsigned short qid=0);
	
	bool isPartofBaseDomain(std::string name);

	void addUnverifiedIPv4Addrs(std::set<std::string>ips);
	void addUnverifiedIPv4Addrs(std::string ip);
	void addVerifiedHosts(std::set<std::string> ips, std::set<std::string> names);
	
	void QuerySender::ExtractAnswersFromRRSet(const isc::dns::Message &msg, std::set<std::string> &ips, std::set<std::string> &names);
	std::string QuerySender::extractName(std::string qname);
	std::string extractIPFromArpa(std::string ip);
	std::string flipAddress(std::string ip_addr);

	void getVerifiedIPsFromQuestions(const isc::dns::Message &msg, std::set<std::string> &ip);
	void getVerifiedNamesFromQuestions(const isc::dns::Message &msg, std::set<std::string> &names);
	
	QuerySender(UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4_NamesAddresses>& verified_hosts_queue,
				UniqueTSPQueue<std::string>& unverified_names_queue, 
				UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4Addresses>& unverified_ips_queue);

	std::string replaceAllSubstrs(std::string str, std::string substr, std::string new_substr);
	/* increment qid value,
		set deadline timer, 
		add timer, query, retrys to pending_*,
		increment the number of qid values */

	void sendNextARecordQuery();
	/* recv the reply, 
		check the qid, 
		stop the pending timer, 
		remove the msg from pending queue, 
		read the ip address,
		get the name of the host, and check it against the base domain name,
		if substring is a match then add them to the verified queue,
		increment the address by the boundary checking each ip to see if it is in the verified_queue_,
		if not in the verified queue, send a reverse lookup*/

	/* send the reverse lookup, 
		increment the qid,
		start a timer for the pending_*,
	*/
	void sendNextPTRRecordQuery();
	void sendDNSQuery(unsigned short qid, std::string qdata);
	
	
	void handleResendTimer(const boost::system::error_code& /*e*/,
							  unsigned short qid);
	void handleAQueryResponseData(const isc::dns::Message &msg);
	void handlePTRQueryResponseData(const isc::dns::Message &msg);

		
	void deleteQQueueInfo(unsigned short qid);
	void deleteQueryData(unsigned short qid);
	void deleteQueryTimer(unsigned short qid);

	void addQQueueInfo(unsigned short qid, boost::asio::mutable_buffer * buf, boost::asio::deadline_timer * timer);
	void addQueryData(unsigned short qid, boost::asio::mutable_buffer * buf);
	void addQueryTimer(unsigned short qid, boost::asio::deadline_timer * timer);

	bool QuerySender::coinFlip();
	

	// used when the clients are used to send all the 
	// requests, and this will parallellize the request
	std::map<unsigned int, SendClientMessage*> tasked_clients_;
	std::set<SendClientMessage*> available_clients_;

	void sendNextPTRRecordQueryClient();
	void sendNextARecordQueryClient();
	void sendQueryWithClient(unsigned short qid, std::string qdata);
	void reclaimName(std::string qdata);
public:
	
	QuerySender(std::string nameserver,
				std::string base_domain,
				UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4_NamesAddresses>& verified_hosts_queue,
				UniqueTSPQueue<std::string>& unverified_names_queue, 
				UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4Addresses>& unverified_ips_queue,
				unsigned short port=53,
				unsigned int ip_boundary=5,
				unsigned short allowed_pending=10,
				unsigned short starting_quid=32768,
				unsigned short max_retrys=10,
				unsigned int qsend_delay_milli=700);
	unsigned short getCurrentQuid();
	bool getSendingStatus();
	void setSendingStatus(bool status=true);
	void getNextNameQueryData(std::string& data);
	std::string peekNextName();
	void sendQueries();
	
	void stop();
	boost::thread * start();
	// client pool threads send out queries, so
	// these are public to send out all the required
	// stuff and handle events clients are implemented in
	// SendClientMessage.cpp
	void sendQueriesWithClients();
	void handleClientMessageTimeout(unsigned int qid);
	void handleClientMessageResponse(isc::dns::Message *msg);
	~QuerySender(void);
	bool isQueriesPending(){
		return !tasked_clients_.empty();
	}

};

typedef boost::shared_ptr<QuerySender> QuerySenderPtr;;