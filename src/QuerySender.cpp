/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
   
#include <string>
#include <iostream>

#include <dns/buffer.h>
#include <dns/question.h>
#include <dns/message.h>
#include <dns/messagerenderer.h>
#include <dns/rrset.h>
#include <dns/exceptions.h>

#include <boost\shared_ptr.hpp>
#include <boost\asio.hpp>
#include <boost\bind.hpp>
#include <boost\regex.hpp>
#include <boost\thread\condition_variable.hpp>
#include <boost\thread\mutex.hpp>
#include <boost\thread\locks.hpp>
#include <boost\date_time\posix_time\posix_time.hpp>
#include <boost\random.hpp>
#include <boost\shared_ptr.hpp>
#include <boost\enable_shared_from_this.hpp>
#include <boost\date_time.hpp>

#include "tspriorityqueue.h"
#include "QuerySender.h"
#include "SendClientMessage.h"

#ifdef UNIX
#include <arpa/inet.h> // XXX: for inet_pton/ntop(), not exist in C++ standards
#include <sys/socket.h> // for AF_INET/AF_INET6
#else
#include <Ws2tcpip.h>
#endif



bool compareByIPv4Address(std::string a, std::string b){
	struct sockaddr_in sa;
	struct sockaddr_in sb;
	if(a.size() == 0){
		return true;
	}
	if(b.size() == 0){
		return false;
	}
	if (a == b){
		return false;
	}
	if (inet_pton(AF_INET, b.c_str(), &sb.sin_addr) != 1)
		return false;
	if (inet_pton(AF_INET, a.c_str(), &sa.sin_addr) != 1)
		return false;
	//sa.sin_addr.S_un.S_addr = ntohl(sa.sin_addr.S_un.S_addr);
	//sb.sin_addr.S_un.S_addr = ntohl(sb.sin_addr.S_un.S_addr);
	if( sa.sin_addr.S_un.S_un_b.s_b1 != sb.sin_addr.S_un.S_un_b.s_b1)
		return sa.sin_addr.S_un.S_un_b.s_b1 < sb.sin_addr.S_un.S_un_b.s_b1;
	if( sa.sin_addr.S_un.S_un_b.s_b2 != sb.sin_addr.S_un.S_un_b.s_b2)
		return sa.sin_addr.S_un.S_un_b.s_b2 < sb.sin_addr.S_un.S_un_b.s_b2;
	if( sa.sin_addr.S_un.S_un_b.s_b3 != sb.sin_addr.S_un.S_un_b.s_b3)
		return sa.sin_addr.S_un.S_un_b.s_b3 < sb.sin_addr.S_un.S_un_b.s_b3;
	return sa.sin_addr.S_un.S_un_b.s_b4 < sb.sin_addr.S_un.S_un_b.s_b4;
}
std::vector<std::string > *tokenize(const std::string& str, std::string del){
	std::vector<std::string > *tokens = new std::vector<std::string > ;
	
	std::string::size_type lastPos = str.find_first_not_of(del,0);
	std::string::size_type currentPos = str.find_first_of(del,lastPos);
	while(std::string::npos != currentPos || std::string::npos != lastPos){
		std::string s = str.substr(lastPos, currentPos - lastPos);
		tokens->push_back(s);
		lastPos = str.find_first_not_of(del,currentPos);
		currentPos = str.find_first_of(del,lastPos);
	}
	return tokens;
}
void clearTokens(std::vector<std::string> *a_tokens, std::vector<std::string> *b_tokens ){
	if (a_tokens){
		a_tokens->clear();
		delete a_tokens;
	}
	if (b_tokens){
		b_tokens->clear();
		delete b_tokens;
	}
	a_tokens = b_tokens = nullptr;
}

bool compareByFQDN(std::string a, std::string b) {
	if(a.size() == 0){
		return true;
	}
	if(b.size() == 0){
		return false;
	}
	if (a == b)
		return false;
	std::vector<std::string> *a_tokens = tokenize(a, ".");
	std::vector<std::string> *b_tokens = tokenize(b, ".");
	bool result = false;
	std::vector<std::string>::reverse_iterator ri_atoks = a_tokens->rbegin();
	std::vector<std::string>::reverse_iterator ri_btoks = b_tokens->rbegin();
	for(; ri_atoks != a_tokens->rend(), ri_btoks != b_tokens->rend(); ri_atoks++, ri_btoks++){
		if( *ri_atoks > *ri_btoks){
			clearTokens(a_tokens, b_tokens);
			return true;
		}
	}
	if(a_tokens->size() > b_tokens->size()){
		result = true;
	}
	clearTokens(a_tokens, b_tokens);
	return result;
}

bool compareByIPv4AndFQDN(std::string a, std::string b){
	if(a.size() == 0){
		return true;
	}
	if(b.size() == 0){
		return false;
	}
	if(a == b)
		return false;

	std::vector<std::string> *a_tokens = tokenize(a, " ");
	std::vector<std::string> *b_tokens = tokenize(b, " ");
	bool result = false;
	if (a_tokens->size() != 2){
		return true;
	}
	if (b_tokens->size() != 2){
		clearTokens(a_tokens, b_tokens);
		return false;
	}
	if ((*a_tokens)[0] != (*b_tokens)[0]){
		result = compareByIPv4Address((*a_tokens)[0], (*b_tokens)[0]);
		clearTokens(a_tokens, b_tokens);
		return result;
	}
	if ((*a_tokens)[1] != (*b_tokens)[1]){
		result = compareByIPv4Address((*a_tokens)[1], (*b_tokens)[1]);
		clearTokens(a_tokens, b_tokens);
		return result;
	}
	clearTokens(a_tokens, b_tokens);
	return result;
}




std::string QuerySender::replaceAllSubstrs(std::string str, std::string substr, std::string new_substr){
	std::string::size_type currentPos = str.find_first_of(substr);
	//std::string new_str = "";
	while(std::string::npos != currentPos){
		str = str.replace(currentPos, substr.size(), new_substr);
		std::string::size_type updatedCurrentPos = currentPos + new_substr.size() - substr.size();
		currentPos =  str.find_first_of(substr, updatedCurrentPos+1 );
	}
	return str;
}

//
bool QuerySender::isIPv4Address(std::string name){
	std::string sreg_ex = "\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b$";
	boost::regex reg_ex(sreg_ex);
	boost::match_results<std::string::const_iterator> what;
	if (0 == boost::regex_match(name, what, reg_ex, boost::match_default | boost::match_partial)){
		return false;
	}
	if (what[0].matched)
		return true;
	return false;
}
bool QuerySender::isPartofBaseDomain(std::string name){
 	std::string sreg_ex = ".*"+replaceAllSubstrs("."+base_domain_, ".", "\\.")+".*";
	boost::regex reg_ex(sreg_ex);
	boost::match_results<std::string::const_iterator> what;
	if (0 == boost::regex_match(name, what, reg_ex, boost::match_default | boost::match_partial)){
		return false;
	}
	if (what[0].matched)
		return true;
	return false;
}

std::string QuerySender::buildARecordQueryDNSMsg(isc::dns::Message &msg, std::string name, unsigned short qid){
	isc::dns::Name n(name);
	isc::dns::RRClass rclass("IN");
	isc::dns::RRType rtype("A");
	isc::dns::Question q(n, rclass, rtype);
	
	msg.setQid(qid);
	msg.setOpcode(isc::dns::Opcode::QUERY());
	msg.addQuestion(q);
	msg.setHeaderFlag(isc::dns::MessageFlag::RD());

	isc::dns::OutputBuffer outbuffer(msg.getUDPSize());
	isc::dns::MessageRenderer renderer(outbuffer);
	msg.toWire(renderer);
	const void *data = renderer.getData();
	unsigned int sz = renderer.getLength();
	std::string d((const char *)data, sz);
	return d;
}

std::string QuerySender::buildPTRRecordQueryDNSMsg(isc::dns::Message &msg, std::string ip_address, unsigned short qid){
	isc::dns::Name n(ip_address);
	isc::dns::RRClass rclass("IN");
	isc::dns::RRType rtype("PTR");
	isc::dns::Question q(n, rclass, rtype);
	
	msg.setQid(qid);
	msg.setOpcode(isc::dns::Opcode::QUERY());
	msg.addQuestion(q);
	msg.setHeaderFlag(isc::dns::MessageFlag::RD());
	
	isc::dns::OutputBuffer outbuffer(msg.getUDPSize());
	isc::dns::MessageRenderer renderer(outbuffer);
	msg.toWire(renderer);
	//std::cout << "Sending the following query: " << msg.toText() << std::endl;
	const void *data = renderer.getData();
	unsigned int sz = renderer.getLength();
	std::string d((const char *)data, sz);
	return d;
}



void QuerySender::handleSendQuery(const boost::system::error_code& e, std::size_t bytes_xfered, unsigned short qid, unsigned int total_bytes_sent){
	if(e){
		std::cout << "Uh-oh Error in handleSendQuery: " << e.message() << std::endl;
		cancelTimer(qid);
		return;
	}
	boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(pending_qmutex_);
	if (pending_qtimers_.size() == 0 || pending_qtimers_.find(qid) == pending_qtimers_.end()){
		// nothing to do, so we shall return
		pq_lock.unlock();
		// cant find the timer, so we just delete the element if it is present
		deleteQQueueInfo(qid);
		return;
	}
	boost::asio::mutable_buffer *qdata_buf = nullptr;
	if (pending_qdata_.size() == 0 || pending_qdata_.find(qid) == pending_qdata_.end()){
		pq_lock.unlock();
		deleteQQueueInfo(qid);
		return;
	}
	boost::unique_lock<boost::mutex> io_lock_(io_mutex_);
	boost::asio::deadline_timer *t = pending_qtimers_[qid];
	if (t == nullptr){
		// the timer is a bad pointer, so we just delete the pending query info
		pq_lock.unlock();
		io_lock_.unlock();
		deleteQQueueInfo(qid);
		return;
	}
	t->expires_from_now( boost::posix_time::milliseconds(4*qsend_delay_milli_));
	t->async_wait(boost::bind(&QuerySender::handleResendTimer,this,
        boost::asio::placeholders::error,  qid));
	io_lock_.unlock();
	pq_lock.unlock();
}
void QuerySender::handleResendTimer(const boost::system::error_code& e,  unsigned short qid){
	
	if(e == boost::asio::error::operation_aborted){
		// if the event is canceled and we do not want to reschedule
		deleteQQueueInfo(qid);
		return;
	}
	else if (e){
		std::cout << "Uh-oh Error in handleReSendTimer: " << e.message() << std::endl;
		return;
	}

	boost::asio::mutable_buffer *qdata_buf = nullptr;
	boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(pending_qmutex_);
	pending_qretrys_[qid]++;
	unsigned int retry_count = (pending_qretrys_[qid]);
	if (pending_qdata_.size() == 0 || pending_qdata_.find(qid) == pending_qdata_.end() || (retry_count) >= max_retrys_){
		pq_lock.unlock();
		// buffer data (pending dns msg) was not in the queue information, so we will delete 
		// the related queue information
		deleteQQueueInfo(qid);
		return;
	}
	qdata_buf = pending_qdata_[qid];
	if(qdata_buf == nullptr){
		// buffer data (pending msg) was deallocated, and 
		// we can't send a null buffer
		pq_lock.unlock();
		deleteQQueueInfo(qid);
		return;
	}
	
	// scoped mutex is released upon return.
	boost::mutex::scoped_lock sl(io_mutex_);
	try{
		socket_.async_send_to(
        	boost::asio::buffer(*qdata_buf), remote_endpoint_,
        	boost::bind(&QuerySender::handleSendQuery, this,boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, qid,0));
	}catch(boost::system::system_error & e) {
		std::cout << "Uh-Oh exception in sendDNSQuery: "  << e.what() << std::endl;
	}
	pq_lock.unlock();
}


void QuerySender::sendDNSQuery(unsigned short qid, std::string qdata){
	waitPendingQueries();
	char *arry = new char[qdata.size()+1];
	memcpy(arry, qdata.c_str(),qdata.size() );
	boost::asio::mutable_buffer *qdata_buf = new boost::asio::mutable_buffer(arry, qdata.size()+1);
	// lock shared queue resources
	boost::unique_lock<boost::mutex> sl(io_mutex_);
	boost::asio::deadline_timer *t = new boost::asio::deadline_timer(io_service_);
	sl.unlock();
	addQQueueInfo(qid,qdata_buf,t);
	try{
		socket_.async_send_to(
	        boost::asio::buffer(*qdata_buf), remote_endpoint_,
			boost::bind(&QuerySender::handleSendQuery, this,boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, qid, 0));
			boost::asio::deadline_timer t(io_service_, boost::posix_time::milliseconds(qsend_delay_milli_));
			t.wait();
	}catch(boost::system::system_error & e) {
		std::cout << "Uh-Oh exception in sendDNSQuery: "  << e.what() << std::endl;
	}
}
	


void QuerySender::sendNextARecordQuery(){
	unsigned short qid = incrQuid();
	std::string name = buildNextName();
	if (name.size() == 0)
		return;
	isc::dns::Message msg(isc::dns::Message::Mode::RENDER);
	std::string qdata = buildARecordQueryDNSMsg(msg, name, qid);
	sendDNSQuery(qid, qdata);
}



void QuerySender::sendNextPTRRecordQuery(){
	if(unverified_ips_queue_.isEmpty())
		return;
	std::string ipaddress = flipAddress(unverified_ips_queue_.popTop());
	if(ipaddress == "")
		return;
	unsigned short qid = incrQuid();
	ipaddress += ".in-addr.arpa";
	isc::dns::Message msg(isc::dns::Message::Mode::RENDER);
	std::string qdata = buildPTRRecordQueryDNSMsg(msg, ipaddress, qid);
	sendDNSQuery(qid, qdata);
}

std::string QuerySender::buildNextName(){
	if(unverified_names_queue_.isEmpty())
		return std::string();
	std::string d = unverified_names_queue_.popTop();
	if (d.size() == 0){
		return d;
	}
	if(isPartofBaseDomain(d)){
		return d;
	}
	return d+"."+base_domain_;
}
std::string QuerySender::peekNextName(){
	std::string d = unverified_names_queue_.top() + "." + base_domain_;
	return d;
}


void QuerySender::addUnverifiedIPv4Addrs(std::set<std::string>ips){
	std::set<std::string>::iterator ip_iter = ips.begin();
	for(;ip_iter != ips.end();ip_iter++){
		addUnverifiedIPv4Addrs(*ip_iter);
	}	
}

void QuerySender::addCheckedAddrs(std::vector<std::string> ip_addresses){
	std::vector<std::string>::iterator ip_iter = ip_addresses.begin();
	for(;ip_iter != ip_addresses.end(); ip_iter++){
		addCheckedAddrs(*ip_iter);
	}
}
void QuerySender::addCheckedAddrs(std::set<std::string> ip_addresses){
	std::set<std::string>::iterator ip_iter = ip_addresses.begin();
	for(;ip_iter != ip_addresses.end(); ip_iter++){
		addCheckedAddrs(*ip_iter);
	}
}
void QuerySender::addCheckedAddrs(std::string ip_address){
	struct sockaddr_in sa;
	if (inet_pton(AF_INET, ip_address.c_str(), &sa.sin_addr) == 1){
		addCheckedAddrs(sa.sin_addr.S_un.S_addr);
	}
}
void QuerySender::addCheckedAddrs(unsigned int ip_address){
	checked_ip_addresses_ >> ip_address;
}
bool QuerySender::isAddrChecked(std::string ip_address){
	struct sockaddr_in sa;
	if (inet_pton(AF_INET, ip_address.c_str(), &(sa.sin_addr)) == 1){
		return isAddrChecked(sa.sin_addr.S_un.S_addr);
	}
	return false;
}
bool QuerySender::isAddrChecked(unsigned int ip_address){
	return checked_ip_addresses_.containsKey(ip_address);
	
}
std::string QuerySender::flipAddress(std::string ip_addr){
	struct sockaddr_in sa;
	char dst[1024] = {0};
	std::string x = "";
	if(inet_pton(AF_INET,ip_addr.c_str(),&sa.sin_addr) != 1)
		return x;
	sa.sin_addr.S_un.S_addr = ntohl(sa.sin_addr.S_un.S_addr);
	inet_ntop(AF_INET,&sa.sin_addr,dst,1024);
	x = std::string(dst);
	return x;
}

void QuerySender::addUnverifiedIPv4Addrs(std::string ip_address){
	struct sockaddr_in sa;
    char dst[1024];
	int result = inet_pton(AF_INET, ip_address.c_str(), &sa.sin_addr);
	if (result <= 0)
		return;
	unsigned int m = ntohl(sa.sin_addr.S_un.S_addr),
				 start = m - ip_address_boundary_/2,
				 end = m + ip_address_boundary_/2;
	
	while(start <= end){
		unsigned int addr = htonl(start);
		if (isAddrChecked(addr)){
			start++;
			continue;
		}
		checked_ip_addresses_ >> start;
		sa.sin_addr.S_un.S_addr = addr;
		if(inet_ntop(AF_INET, &sa.sin_addr, dst, 1024)){
			std::string host(dst);
			//std::cout << "Adding host to the unverified queue: " << host << std::endl;
			unverified_ips_queue_ >> host;
		}
		*dst = '\0';
		start++;
	}
}


void QuerySender::getVerifiedNamesFromQuestions(const isc::dns::Message &msg, std::set<std::string> &names){
	isc::dns::QuestionIterator question_i = msg.beginQuestion(),
								end_q = msg.endQuestion();
	do {
		std::string name = extractName((*question_i)->getName().toText());
		if (!isPartofBaseDomain(name))
			continue;
		names.insert(name);
		question_i++;
	}while(question_i != end_q );
}

std::string QuerySender::extractIPFromArpa(std::string ip){
	std::string arpa_str = ".in-addr.arpa";
	unsigned int pos;
	if((pos = ip.find(arpa_str)) == std::string::npos){
		return std::string("");
	}
	// remove .in-addr.arpa
	ip = ip.substr(0,pos);
	ip = flipAddress(ip);
	return ip;
}


void QuerySender::getVerifiedIPsFromQuestions(const isc::dns::Message &msg, std::set<std::string> &ips){
	isc::dns::QuestionIterator question_i = msg.beginQuestion(),
					question_end = msg.endQuestion();
	do {
		std::string ip = (*question_i)->getName().toText();
		ip = extractIPFromArpa(ip);
		if(ip.size() > 0)
			ips.insert(ip);
		question_i++;
	}while(question_i != msg.endQuestion());
}

std::string QuerySender::extractName(std::string qname){
	std::string d(qname.c_str(), qname.size()-1);
	return d;
}

void QuerySender::ExtractAnswersFromRRSet(const isc::dns::Message &msg, std::set<std::string> &ips, std::set<std::string> &names){
	isc::dns::RRsetIterator answers_i = msg.beginSection(isc::dns::Section::ANSWER()),
					answers_end = msg.endSection(isc::dns::Section::ANSWER());
	for (;answers_i !=  answers_end; answers_i++){
		isc::dns::RdataIteratorPtr rdata_ip  = (*answers_i)->getRdataIterator();
		rdata_ip->first();
		if ((*answers_i)->getType().toText() == "CNAME"){
			bool found_one = false;
			std::string cname = extractName((*answers_i)->getName().toText());
			for(;!rdata_ip->isLast();rdata_ip->next()){
				std::string name = extractName(rdata_ip->getCurrent().toText());
				if (isPartofBaseDomain(name)){
					names.insert(name);
					found_one = true;
				}
			}
			if (found_one)
				names.insert(cname);
		}else if((*answers_i)->getType().toText() == "A"){
			std::string name = extractName((*answers_i)->getName().toText());
			if (!isPartofBaseDomain(name)||(names.find(name) ==  names.end()))
				continue;
			names.insert(name);
			for(;!rdata_ip->isLast();rdata_ip->next()){
				const isc::dns::rdata::Rdata& rdata_v = rdata_ip->getCurrent();
				std::string s = rdata_v.toText();
				ips.insert(s);
			}
		}else if((*answers_i)->getType().toText() == "PTR"){
			ips.insert(extractIPFromArpa((*answers_i)->getName().toText()));
			for(;!rdata_ip->isLast();rdata_ip->next()){
				const isc::dns::rdata::Rdata& rdata_v = rdata_ip->getCurrent();
				std::string name = extractName(rdata_v.toText());
				if (isPartofBaseDomain(name))
					names.insert(name);
			}
		}
	}
}



void QuerySender::receiveData(){
	boost::array<char, isc::dns::Message::DEFAULT_MAX_UDPSIZE> *recv_buf = nullptr;
	isc::dns::Message *msg = nullptr;
	// return if there are no pending requests;
	boost::asio::ip::udp::endpoint sender_endpoint = remote_endpoint_;
	
	while(getPendingCount() || continueSending_){
		try{
			recv_buf = new boost::array<char, isc::dns::Message::DEFAULT_MAX_UDPSIZE>();
			isc::dns::Message *msg = new isc::dns::Message(isc::dns::Message::Mode::PARSE);
			msg->clear(isc::dns::Message::Mode::PARSE);
			if (recv_buf == nullptr || msg == nullptr){
				std::cout << "WTF recv_buf is null!" << std::endl;
			}
			socket_.receive_from(boost::asio::buffer(*recv_buf), sender_endpoint);
			isc::dns::InputBuffer inbuffer(recv_buf->data(), recv_buf->size());
			msg->fromWire(inbuffer);
			unsigned short qid = msg->getQid();
			cancelTimer(qid);
			threads_.create_thread(boost::bind(&QuerySender::handleReceive, this, msg));
		}catch( boost::system::system_error & e) {
			std::cout << "Uh-Oh exception in receiveData: "  << e.what() << std::endl;
			
		} catch(const isc::dns::MessageTooShort e){
		/* need to implement read_some here */
			std::cout << e.what();
		}catch(const isc::dns::InvalidMessageSection e){
			/* need to implement read_some here */
			std::cout << e.what();
		}
		delete recv_buf;
		recv_buf = nullptr;
	}
}

void QuerySender::handleReceive(isc::dns::Message *msg){
		// try-catch blocks here, the  response may not be complete,
		// or invalid
	try{
		unsigned short qid = msg->getQid();
		isc::dns::QuestionIterator q_itr = msg->beginQuestion();
		isc::dns::QuestionIterator q_end = msg->endQuestion();
		if (q_itr == q_end){
			delete msg;
			msg = nullptr;
			return;
		}
		isc::dns::QuestionPtr q_ptr = *q_itr;
		if (q_ptr->getType().toText() == "PTR"){
			handlePTRQueryResponseData(*msg);
		}else if(q_ptr->getType().toText() == "A" || q_ptr->getType().toText() == "CNAME"){
			handleAQueryResponseData(*msg);
		}

	}catch(const isc::dns::MessageTooShort e){
		/* need to implement read_some here */
		std::cout << e.what();
	}catch(const isc::dns::InvalidMessageSection e){
		/* need to implement read_some here */
		std::cout << e.what();
	}
	delete msg;
	msg = nullptr;
}
void QuerySender::addVerifiedHosts(std::set<std::string> ips, std::set<std::string> names){
	std::set<std::string>::iterator ip_i = ips.begin();
	std::set<std::string>::iterator names_i = names.begin();
	for(; ip_i != ips.end(); ip_i++){
		for(names_i = names.begin(); names_i != names.end(); names_i++){
			std::string name = *ip_i +" "+*names_i;
			if (!verified_hosts_queue_.containsKey(name)){
				verified_hosts_queue_.push(name);
				std::cout << "Adding the following host: " << name << std::endl;
			}
		}
	}
}


void QuerySender::handleAQueryResponseData(const isc::dns::Message &msg){
	std::set<std::string> ips;
	std::set<std::string> names;
	isc::dns::RRsetIterator answers_i = msg.beginSection(isc::dns::Section::ANSWER()),
						   end = msg.endSection(isc::dns::Section::ANSWER());
	// nothing to do if there are no answers
	if (answers_i ==  end || (*answers_i)->getRdataCount() == 0){
		return;
	}
	getVerifiedNamesFromQuestions(msg, names);
	ExtractAnswersFromRRSet(msg, ips, names);
	// commented this out, because i want to know if an IP may have more
	// than one name associated with it, and by not adding it to the 
	// checked addressesses we force a PTR lookup, which will add it 
	// to the set
	//addCheckedAddrs(ips);
	addVerifiedHosts(ips, names);
	addUnverifiedIPv4Addrs(ips);
}

void QuerySender::handlePTRQueryResponseData(const isc::dns::Message &msg){
	std::set<std::string> ips;
	std::set<std::string> names;
	isc::dns::RRsetIterator answers_i = msg.beginSection(isc::dns::Section::ANSWER()),
		end = msg.endSection(isc::dns::Section::ANSWER());
	//isc::dns::RRsetPtr i = *answers_i;
	if (answers_i == end || (*answers_i)->getRdataCount() == 0){
		return;
	}
	getVerifiedIPsFromQuestions(msg, ips);
	ExtractAnswersFromRRSet(msg, ips, names);
	addCheckedAddrs(ips);
	addVerifiedHosts(ips, names);
}

unsigned short QuerySender::incrQuid(){
	boost::mutex::scoped_lock s(quid_mutex_);
	return (++current_quid_);
}

unsigned short QuerySender::getCurrentQuid(){
	boost::mutex::scoped_lock s(quid_mutex_);
	return current_quid_;
}

QuerySender::QuerySender(UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4_NamesAddresses>& verified_hosts_queue,
				UniqueTSPQueue<std::string>& unverified_names_queue, 
				UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4Addresses>& unverified_ips_queue):
			// start with initilizations
					qsend_delay_milli_(800),
					nameserver_(""), 
					base_domain_(""), 
					verified_hosts_queue_(verified_hosts_queue),
					unverified_names_queue_(unverified_names_queue),
					unverified_ips_queue_(unverified_ips_queue),
					port_(0),
					ip_address_boundary_(0),
					max_pending_queries_(0),
					current_quid_(0),
					max_retrys_(0),
					quid_mutex_(),
					pending_qmutex_(),
					pending_queries_cnd_(),
					pending_qdata_(),
					pending_qtimers_(),
					io_service_(),
					socket_(io_service_),
					remote_endpoint_(boost::asio::ip::address_v4::from_string(nameserver_), port_),
					rcvr_thread(nullptr),
					io_mutex_(),
					queries_cnt_mutex_()
{
	socket_.open(boost::asio::ip::udp::v4());
}

QuerySender::QuerySender(std::string nameserver,
			std::string base_domain,
			UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4_NamesAddresses>& verified_hosts_queue,
			UniqueTSPQueue<std::string>& unverified_names_queue, 
			UniqueTSPQueue<std::string, std::vector<std::string>, CompareIPv4Addresses>& unverified_ips_queue,
			unsigned short port,
			unsigned int ip_boundary,
			unsigned short allowed_pending,
			unsigned short starting_quid,
			unsigned short max_retrys,
			unsigned int qsend_delay_milli):
			// start with initilizations
					qsend_delay_milli_(qsend_delay_milli),
					nameserver_(nameserver), 
					base_domain_(base_domain), 
					verified_hosts_queue_(verified_hosts_queue),
					unverified_names_queue_(unverified_names_queue),
					unverified_ips_queue_(unverified_ips_queue),
					port_(port),
					ip_address_boundary_(ip_boundary),
					max_pending_queries_(allowed_pending),
					current_quid_(starting_quid),
					max_retrys_(max_retrys),
					quid_mutex_(),
					pending_qmutex_(),
					pending_queries_cnd_(),
					pending_qdata_(),
					pending_qtimers_(),
					io_service_(),
					socket_(io_service_),
					rcvr_thread(nullptr),
					io_mutex_(),
					queries_cnt_mutex_(),
					available_clients_(),
					tasked_clients_(),
					remote_endpoint_()
{
	boost::asio::ip::tcp::resolver::query query( nameserver, "0");
	boost::asio::ip::tcp::resolver resolver( io_service_ );
	boost::system::error_code error;
	boost::asio::ip::tcp::resolver::iterator iter = resolver.resolve(query, error);
	boost::asio::ip::tcp::resolver::iterator end;       
	if ( error.value() == 0 && iter != end){
		remote_endpoint_ = boost::asio::ip::udp::endpoint(iter->endpoint().address(), port_);
		nameserver_ = iter->endpoint().address().to_string();
		socket_.open(boost::asio::ip::udp::v4());
	}
}


unsigned int QuerySender::getPendingCount(){
	boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(pending_qmutex_);
	unsigned int x = pending_qdata_.size();
	pq_lock.unlock();
	return x;
}

void QuerySender::deleteQQueueInfo(unsigned short qid){
	boost::interprocess::upgradable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(pending_qmutex_);
	deleteQueryTimer(qid);
	deleteQueryData(qid);
	pq_lock.unlock();
	pending_queries_cnd_.notify_all();
}

void QuerySender::addQQueueInfo(unsigned short qid, boost::asio::mutable_buffer * buf, boost::asio::deadline_timer * timer){
	boost::interprocess::upgradable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(pending_qmutex_);
	addQueryTimer(qid, timer);
	addQueryData(qid, buf);
	pq_lock.unlock();
	pending_queries_cnd_.notify_all();
}

void QuerySender::deleteQueryData(unsigned short qid){
	// need a scoped mutex here, but we rely on the caller to have it
	std::map<unsigned short, boost::asio::mutable_buffer *>::iterator t = pending_qdata_.find(qid);
	if (t == pending_qdata_.end() && t->second != nullptr){
		// timer never made it into pending queues, uh-oh
		return;
	}
	char *arry = boost::asio::buffer_cast<char *>(*t->second);
	delete arry;
	arry = nullptr;
	delete t->second;
	t->second = nullptr;
	pending_qdata_.erase(t->first);
}

void QuerySender::deleteQueryTimer(unsigned short qid){
	// need a scoped mutex here, but we rely on the caller to have it
	std::map<unsigned short, boost::asio::deadline_timer *>::iterator t = pending_qtimers_.find(qid);
	if (t != pending_qtimers_.end() && t->second != nullptr){
		delete t->second;
		t->second = nullptr;
		pending_qtimers_.erase(t->first);
	}
	// this may be causing problems because of the
	// the cancel process causes an operation aborted
	
	std::map<unsigned short, unsigned int>::iterator r = pending_qretrys_.find(qid);
	if (r != pending_qretrys_.end()){
		// timer never made it into pending queues, uh-oh
		pending_qretrys_.erase(r->first);
	}
}

void QuerySender::cancelTimer(unsigned short qid){
	boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(pending_qmutex_);
	std::map<unsigned short, boost::asio::deadline_timer *>::iterator t = pending_qtimers_.find(qid);
	if (t == pending_qtimers_.end()){
		// timer never made it into pending queues, uh-oh
		return;
	}
	t->second->cancel();
	pq_lock.unlock();
}

void QuerySender::addQueryData(unsigned short qid, boost::asio::mutable_buffer * buf){
	// we rely on the caller to have it, otherwise we need a scoped mutex here, 
	pending_qdata_[qid] = buf;
}
void QuerySender::addQueryTimer(unsigned short qid, boost::asio::deadline_timer * timer){
	// we rely on the caller to have it, otherwise we need a scoped mutex here, 
	pending_qtimers_[qid] = timer;
	pending_qretrys_[qid] = 0;
}

void QuerySender::wakeupPendingQueriesThreads(const boost::system::system_error e){
	if(e.code() == boost::asio::error::operation_aborted )
		return;
	boost::system::error_code ecode();
	const boost::system::system_error ec();
	boost::asio::deadline_timer t(io_service_, boost::posix_time::milliseconds(4*qsend_delay_milli_));
	pending_qmutex_.lock();
	// kick start the timer that is at the beginning of pending_qdata_, 
	if (pending_qdata_.begin() != pending_qdata_.end() ){
		t.async_wait(boost::bind(&QuerySender::handleResendTimer,this,  boost::asio::placeholders::error, pending_qdata_.begin()->first));
	}
	pending_qmutex_.unlock();
	pending_queries_cnd_.notify_one();
}

void QuerySender::waitPendingQueries(){
	
	while (getPendingCount() >= max_pending_queries_ ){
		boost::unique_lock<boost::mutex> pq_lock(queries_cnt_mutex_, boost::posix_time::seconds(5));
		// wait will release the lock automatically, here we just register a 
		// a wake-up call
		boost::unique_lock<boost::mutex> ul(io_mutex_);
		boost::asio::deadline_timer t(io_service_, boost::posix_time::milliseconds(6*qsend_delay_milli_));
		t.async_wait(boost::bind(&QuerySender::wakeupPendingQueriesThreads,this,  boost::asio::placeholders::error));
		ul.unlock();
		pending_queries_cnd_.wait(pq_lock);
		t.cancel();


	}
	pending_queries_cnd_.notify_all();
}

bool QuerySender::coinFlip(){
	static boost::mt19937 rng;
	static boost::uniform_int<> two(1,2);
	static boost::variate_generator<boost::mt19937&, boost::uniform_int<> > die(rng, two);
	return die() %2 == 1;

}
bool QuerySender::getSendingStatus(){
	return continueSending_;
}
void QuerySender::setSendingStatus(bool status){
	continueSending_ = status;
}

void QuerySender::sendQueries(){
	boost::asio::deadline_timer t(io_service_);
	continueSending_ = true;
	if (rcvr_thread == nullptr)
		rcvr_thread = new boost::thread(boost::bind(&QuerySender::receiveData, this));
	// run is the io_service_.run(), and that needs to be run
	// in an indefinite, sentinel loop, or it may exit prematurely
	threads_.create_thread(boost::bind(&QuerySender::run, this));	
	while(continueSending_){
		if (unverified_ips_queue_.isEmpty() &&
			unverified_names_queue_.isEmpty()){
				t.expires_from_now(boost::posix_time::seconds(3));
		}
		if (!unverified_names_queue_.isEmpty()){
			sendNextARecordQuery();
		}
		if (!unverified_ips_queue_.isEmpty()){
			sendNextPTRRecordQuery();
		}
	}
	io_service_.stop();

}


void QuerySender::stop(){
	continueSending_ = false;
	boost::asio::deadline_timer t(io_service_);
	boost::mutex::scoped_lock sl(queries_cnt_mutex_);
	std::map<unsigned int, SendClientMessage *>::iterator titer = tasked_clients_.begin();
	for(;titer != tasked_clients_.end(); titer++){
		titer->second->cancel();
		available_clients_.insert(titer->second);
	}
	tasked_clients_.clear();
	io_service_.stop();
}

boost::thread * QuerySender::start(){
	boost::thread *t = new boost::thread(boost::bind(&QuerySender::sendQueriesWithClients, this));
	return t;
}

void QuerySender::run(){
	try{
		while(continueSending_)
			io_service_.run();
	}catch(const boost::system::system_error &e){
		std::cout << "Uh-oh: io_service had an error: " << e.what() << std::endl;
	}
}

void QuerySender::reclaimName(std::string qdata){
	isc::dns::Message msg(isc::dns::Message::Mode::PARSE);
	isc::dns::InputBuffer inbuffer(qdata.c_str(), qdata.size());
	isc::dns::QuestionIterator question_i = msg.beginQuestion();
	std::string name = extractName((*question_i)->getName().toText());
	unverified_names_queue_.push(name);
}

void QuerySender::handleClientMessageResponse(isc::dns::Message *msg){
	
	// mark data processed
	SendClientMessage* clt = nullptr;
	
	unsigned short qid = msg->getQid();
	//std::cout << "Handling Msg: "<< qid << std::endl;
	boost::unique_lock<boost::mutex> sl(queries_cnt_mutex_);
	if(tasked_clients_.size() == 0 || tasked_clients_.find(qid) == tasked_clients_.end()){
		std::cout << "Fail, looks like a Client was tasked without being added to the tasking list!\n";
		// likely a duplicate message so we will go ahead and process the message
		sl.unlock();
		delete msg;
		return;
	}else{
		clt = tasked_clients_[qid];
		tasked_clients_.erase(qid);
		available_clients_.insert(clt);
		clt->dataProcessed();
		pending_queries_cnd_.notify_all();
	}
	sl.unlock();
	handleReceive(msg);
}
void QuerySender::handleClientMessageTimeout(unsigned int qid){
	// mark data processed
	SendClientMessage* clt = nullptr;
	boost::unique_lock<boost::mutex> sl(queries_cnt_mutex_);
	if(tasked_clients_.size() == 0 || tasked_clients_.find(qid) == tasked_clients_.end()){
		std::cout << "Fail, looks like a Client was tasked without being added to the tasking list!\n";
		sl.unlock();
		return;
	}else{
		clt = tasked_clients_[qid];
		tasked_clients_.erase(qid);
		available_clients_.insert(clt);
		clt->dataProcessed();
	}
	sl.unlock();
	pending_queries_cnd_.notify_all();
}

void QuerySender::sendQueryWithClient(unsigned short qid, std::string qdata){
	boost::mutex::scoped_lock sl(queries_cnt_mutex_);
	std::set<SendClientMessage*>::iterator it;
	SendClientMessage* clt = nullptr;
	while(available_clients_.empty()){
		// wait 
		pending_queries_cnd_.wait(sl);
	}
	clt = *available_clients_.begin();
	available_clients_.erase(*available_clients_.begin());
	tasked_clients_[qid] = clt;
	//sl.unlock();
	if(!clt->sendQuery(qdata, qid)){
		available_clients_.insert(clt);
		tasked_clients_.erase(qid);
		std::cout << "Failed to send the query for qid: " << qid << std::endl;
		//reclaimName(qdata);
	}
}
void QuerySender::sendQueriesWithClients(){
	unsigned int clients_cnt = max_pending_queries_ > 1000 ? 999 :max_pending_queries_;
	if(!tasked_clients_.empty()){
		std::map<unsigned int, SendClientMessage *>::iterator it = tasked_clients_.begin();
		for(;it != tasked_clients_.end(); it++){
			available_clients_.insert(it->second);
		}
		tasked_clients_.clear();
	}
	if(clients_cnt > available_clients_.size()){
		for(unsigned int i = available_clients_.size(); i <= clients_cnt; i++){
			available_clients_.insert((new SendClientMessage(io_service_,nameserver_,port_,*this,qsend_delay_milli_,max_retrys_)));
		}
	}

	boost::asio::deadline_timer t(io_service_);
	continueSending_ = true;
	// run is the io_service_.run(), and that needs to be run
	// in an indefinite, sentinel loop, or it may exit prematurely
	threads_.create_thread(boost::bind(&QuerySender::run, this));
	while(continueSending_){
		if (unverified_ips_queue_.isEmpty() &&
			unverified_names_queue_.isEmpty()){
				t.expires_from_now(boost::posix_time::seconds(3));
				t.wait();
		}
		if (!unverified_names_queue_.isEmpty()){
			sendNextARecordQueryClient();
		}
		if (!unverified_ips_queue_.isEmpty()){
			sendNextPTRRecordQueryClient();
		}
	}
	
	
}

void QuerySender::sendNextARecordQueryClient(){
	unsigned short qid = incrQuid();
	std::string name = buildNextName();
	if (name.size() == 0)
		return;
	isc::dns::Message msg(isc::dns::Message::Mode::RENDER);
	std::string qdata = buildARecordQueryDNSMsg(msg, name, qid);
	sendQueryWithClient(qid, qdata);
}

void QuerySender::sendNextPTRRecordQueryClient(){
	if(unverified_ips_queue_.isEmpty())
		return;
	std::string ipaddress = flipAddress(unverified_ips_queue_.popTop());
	if(ipaddress == "")
		return;
	unsigned short qid = incrQuid();
	ipaddress += ".in-addr.arpa";
	isc::dns::Message msg(isc::dns::Message::Mode::RENDER);
	std::string qdata = buildPTRRecordQueryDNSMsg(msg, ipaddress, qid);
	sendQueryWithClient(qid, qdata);
}


QuerySender::~QuerySender(void){
	stop();
	std::set<SendClientMessage *>::iterator it2 = available_clients_.begin();
	for(;it2 != available_clients_.end();it2++){
		delete *it2;
	}
	available_clients_.clear();
}
