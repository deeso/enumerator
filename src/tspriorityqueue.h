/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#ifndef __TSPRIORITYQUEUE_H__
#define __TSPRIORITYQUEUE_H__

#include <string>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <queue>
#include <memory>

#include <boost/thread/mutex.hpp>

#include <boost/interprocess/sync/interprocess_upgradable_mutex.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>


template <class T, 
		class Container = std::vector<T>, 
		class Compare = std::less<typename Container::value_type> >
class TSPQueue {
	boost::mutex mutex_;
	std::priority_queue<T,Container,Compare> m_pqueue;
	
public:
	TSPQueue():m_pqueue(),mutex_(){
	
	}
	~TSPQueue(){
	
	}
	void clear(){
		boost::interprocess::upgradable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		while(!m_pqueue.empty()){
			m_pqueue.pop();
		}
		pq_lock.unlock();
	}
	void push(const T &b){
		boost::interprocess::upgradable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		m_pqueue.push(b);
		pq_lock.unlock();
	}
	T top(){
		boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		T x = m_pqueue.top();
		pq_lock.unlock()
		return x;

	}
	void pop(){
		boost::interprocess::upgradable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		if (m_pqueue.empty()) return;
		T k = m_pqueue.top();
		m_pqueue.pop();
		pq_lock.unlock()
	}
	T popTop(){
		boost::interprocess::upgradable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		T x = m_pqueue.top();
		m_pqueue.pop();
		pq_lock.unlock();
		return x;

	}
	void operator>>(T & value){
		push(value);
	}
	void operator<<(T &value){
		value = popTop();
	}
	bool isEmpty() {
		boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		bool x = m_pqueue.empty();
		pq_lock.unlock()
		return x;
	}
	unsigned int size() {
		boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		unsigned int x =  m_pqueue.size();
		pq_lock.unlock()
		return x;

	}
};

template <class T, 
		class Container = std::vector<T>, 
		class Compare = std::less<typename Container::value_type>,
		class Allocator = std::allocator<T>>
class UniqueTSPQueue {
	boost::mutex mutex_;
	std::priority_queue<T,Container,Compare> m_pqueue;
	std::set<T,Compare,Allocator> m_unique;
	boost::interprocess::interprocess_upgradable_mutex upgradable_mutex_;
public:
	UniqueTSPQueue():m_pqueue(),mutex_(),upgradable_mutex_(){
	
	}
	~UniqueTSPQueue(){
	
	}
	void clear(){
		boost::interprocess::upgradable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		while(!m_pqueue.empty()){
			m_pqueue.pop();
		}
		m_unique.clear();
		pq_lock.unlock();
	}
	void push(const T &b){
		boost::interprocess::upgradable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		if (m_unique.end() == m_unique.find(b)){
			m_pqueue.push(b);
			m_unique.insert(b);
		}
		pq_lock.unlock();
	}
	T top(){
		boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		T x = m_pqueue.top();
		pq_lock.unlock();
		return x;

	}
	void pop(){
		boost::interprocess::upgradable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		if (m_pqueue.empty()) return;
		T k = m_pqueue.top();
		if (m_unique.find(key) != m_unique.end())
			m_unique.erase(k);
		m_pqueue.pop();
		pq_lock.unlock();
	}
	T popTop(){
		boost::interprocess::upgradable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		T x = m_pqueue.top();
		m_pqueue.pop();
		m_unique.erase(x);
		pq_lock.unlock();
		return x;

	}
	void operator>>(T & value){
		push(value);
	}
	void operator<<(T &value){
		value = popTop();
	}
	bool isEmpty() {
		boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		bool x = m_pqueue.empty();
		pq_lock.unlock();
		return x;
	}
	bool containsKey(T key){
		boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		assert(&m_unique);
		bool x = m_unique.find(key) != m_unique.end();
		pq_lock.unlock();
		return x;
	}
	unsigned int size() {
		boost::interprocess::sharable_lock<boost::interprocess::interprocess_upgradable_mutex> pq_lock(upgradable_mutex_);
		unsigned int x =  m_pqueue.size();
		pq_lock.unlock();
		return x;

	}
};

#endif