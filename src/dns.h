/*

   Copyright 2013 Adam Pridgen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

#ifndef __DNS_H__
#define __DNS_H__

#include <vector>
#include <string>
// Structures are from :
// filename: DNS_QRY.c by Bob Quinn, 1997 


class Question{

	
	void Question::parseData(std::string data);
	void Question::parseData(char * data, unsigned int sz);
	std::vector<std::string > *Question::tokenize( std::string del=".");
public:
	explicit Question::Question():qname(""), qtype(0),qclass(0){}
	explicit Question(std::string name, unsigned short type, unsigned short klass):
		qname(name),qtype(type),qclass(klass){}
		explicit Question(std::string data);
	explicit Question(char *data, unsigned int sz);
	
	~Question(){}
	std::string serialize();
	
	std::string qname;
	unsigned short qtype;
	unsigned short qclass;
};


#endif